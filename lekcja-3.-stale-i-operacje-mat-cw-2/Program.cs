﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lekcja_3._stale_i_operacje_mat_cw_2
{
    class Program
    {
        static void Main(string[] args)
        {
            const int d = 5;
            const int e = 10;
            const int f = 21;

            int wynik = 4 * d + 3 - e * 7 - f;
            Console.WriteLine("Wynik: {0}", wynik);

            Console.ReadKey();
        }
    }
}
